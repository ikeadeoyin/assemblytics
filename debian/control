Source: assemblytics
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/med-team/assemblytics
Vcs-Git: https://salsa.debian.org/med-team/assemblytics.git
Homepage: http://assemblytics.com/
Rules-Requires-Root: no

Package: assemblytics
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         python3,
         python3-numpy,
         r-base-core,
         r-cran-ggplot2,
         r-cran-plyr,
         r-cran-rcolorbrewer
Recommends: mummer
Description: detect and analyze structural variants from a genome assembly
 Assemblytics incorporates a unique anchor filtering approach to increase
 robustness to repetitive elements, and identifies six classes of variants
 based on their distinct alignment signatures. Assemblytics can be applied
 both to comparing aberrant genomes, such as human cancers, to a reference,
 or to identify differences between related species.
